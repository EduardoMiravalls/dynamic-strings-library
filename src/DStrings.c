/*
 * File:    DStrings.c
 * Author:  Eduardo Miravalls Sierra          <edu.miravalls@hotmail.com>
 *
 * Date:    2014-08-30 09:47
 */

/*
 * Dynamic C Strings library.
 * Copyright (C) 2014 Eduardo Miravalls Sierra
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */

#include <stdlib.h>
#include <assert.h>

#include "DStrings.h"

enum {NO = 0, YES};

struct string {
	char *raw;     /**< raw string */
	size_t len;    /**< includes trailing \0 */
	size_t size;   /**< allocated space */
	char resizable;
};

#define resizable(s) ((s)->resizable)

/**
 * @brief allocates an empty string.
 *
 * @return a String.
 */
static String String_alloc(void);

/**
 * @brief resizes s so it can hold at least n chars.
 * @detail swaps s' old raw string for the new one, and updates its size
 * variable.
 *
 * @param s String.
 * @param size new minimum capacity.
 *
 * @return NULL on error or if s is not resizable.
 * @return if s' old raw string was NULL, the newly allocated buffer.
 * @return the old raw string otherwise.
 */
static char *resize(String s, size_t n);

/**
 * @brief resizes s so it can hold at least n chars and copies upto keep
 * bytes from old to new, depending on s->len.
 * @details It doesn't free the old raw string in case it's a source buffer
 * in a copy operation.
 *
 * @param s String.
 * @param size new minimum capacity.
 * @param keep of the first byte that should be left uninitialized.
 *
 * @return NULL on error or if s is not resizable.
 * @return if s' old raw string was NULL, the newly allocated buffer.
 * @return the old raw string otherwise.
 */
static char *resize_and_cpy(String s, size_t n, size_t keep);

/**
 * @brief the name says exactly what it does.
 *
 * @param num
 *
 * @return num's next power of 2.
 * @return if num is a power of 2, it returns it.
 */
static size_t round_up_to_the_next_power_of_2(size_t num);

#if !(_XOPEN_SOURCE >= 700 || _POSIX_C_SOURCE >= 200809L)
/**
 * @brief strlen(s) that returns if it reaches n.
 *
 * @param s raw string.
 * @param n maximum number of bytes to test.
 *
 * @return min(n, strlen(s))
 */
static size_t strnlen(const char *s, size_t n);
#endif

int String_ncpy_at(String dest, size_t dest_offset,
                   const void *src, size_t n)
{
	void *old_raw = NULL;

	assert(dest != NULL);
	assert(src != NULL || n == 0);

	if (dest->size < (dest_offset + n + 1)) {
		old_raw = resize_and_cpy(dest, dest_offset + n + 1, dest_offset);

		if (NULL == old_raw) {
			return -1;
		}

		if (old_raw == dest->raw) {
			old_raw = NULL;
		}
	}

	memmove(dest->raw + dest_offset, src, n);
	dest->len = n + dest_offset + 1;
	dest->raw[n + dest_offset] = '\0';
	free(old_raw);
	return 0;
}

int String_ncat(String dest,
                const void *src, size_t n)
{
	assert(dest != NULL);
	assert(src != NULL);

	return String_ncpy_at(dest, dest->len - 1, src, n);
}

size_t String_length(String s)
{
	assert(s != NULL);
	return s->len;
}

size_t String_size(String s)
{
	assert(s != NULL);
	return s->size;
}

int String_set_size(String s, size_t size)
{
	void *temp;
	assert(s != NULL);

	if (!resizable(s)) {
		return -1;
	}

	if (size == s->size) { /* no changes */
		return 0;
	}

	if (size < 2) {
		size = 1;
	}

	if ((temp = realloc(s->raw, size)) == NULL) {
		return -1;
	}

	s->size = size;
	s->raw = temp;

	if (s->len > size) {
		s->len = size;
		s->raw[size - 1] = '\0';
	}

	return 0;
}

String String_dup_slice(String s, size_t from, size_t to)
{
	String cpy;

	assert(s != NULL);
	assert(from <= to);

	if ((cpy = String_alloc()) == NULL) {
		return NULL;
	}

	if (String_ncpy_at(cpy, 0, s->raw + from, to - from + 1)) {
		String_free(&cpy);
	}

	return cpy;
}

int String_ncmp(String s1, String s2, size_t n)
{
	assert(s1 != NULL);
	assert(s2 != NULL);

	return strncmp(String_raw(s1), String_raw(s2), n);
}

String String_new(const char *src, size_t n)
{
	String s;

	if ((s = String_alloc()) == NULL) {
		return NULL;
	}

	if (src != NULL) {
		n = strnlen(src, n);

	} else {
		n = 0;
	}

	if (String_ncpy_at(s, 0, src, n)) {
		String_free(&s);
	}

	return s;
}

String String_new_steal(char *src, size_t size)
{
	String s;

	if ((s = String_alloc()) == NULL) {
		return NULL;
	}

	if (src != NULL) {
		s->len = strlen(src) + 1;

	} else {
		s->len = 0;
	}

	if (size < s->len) {
		size = s->len;
		s->resizable = NO;
	}

	s->size = size;
	s->raw = src;
	return s;
}

void String_free(String *s)
{
	assert(s != NULL);

	if (*s != NULL) {
		if (resizable(*s)) {
			free((*s)->raw);
		}

		free(*s);
		*s = NULL; /* avoid YADF */
	}
}

#if _BSD_SOURCE || _XOPEN_SOURCE >= 500 || _ISOC99_SOURCE || _POSIX_C_SOURCE >= 200112L

#include <stdio.h>
#include <stdarg.h>

int String_format_at(String s, size_t offset, const char *fmt, ...)
{
	void *old_raw;
	va_list vargs;
	size_t bytes_written;

	assert(s != NULL);
	assert(fmt != NULL);

	if (offset >= s->size) {
		size_t temp = round_up_to_the_next_power_of_2(offset);

		if ((old_raw = resize_and_cpy(s, temp, offset)) == NULL) {
			return -1;
		}

		free(old_raw);
	}

	/* let's see if we're lucky */
	va_start(vargs, fmt);
	bytes_written = vsnprintf(s->raw + offset, s->size - offset, fmt, vargs) + 1;
	va_end(vargs);

	if (bytes_written > s->size) {
		/* woops */
		old_raw = resize_and_cpy(s, offset + bytes_written + 1, offset);

		if (NULL == old_raw) {
			return -1;
		}

		free(old_raw);

		/* ok, second try... */
		va_start(vargs, fmt);
		bytes_written = vsnprintf(s->raw + offset, s->size - offset, fmt, vargs) + 1;
		va_end(vargs);

		if (bytes_written > s->size) {
			/* uh!? */
			return -1;
		}
	}

	s->len = offset + bytes_written;
	return 0;
}

#endif

int String_read(String s, size_t offset,
                FILE *f,
                char const *delim)
{
	char buffer[] = {'\0', '\0'};
	char found = NO;

	assert(s != NULL);

	if (String_ncpy_at(s, offset, buffer, 0)) {
		return -1;
	}

	while(!found) {
		if ((buffer[0] = fgetc(f)) != EOF) {
			if (String_ncat(s, buffer, 1)) {
				break;
			}

			if (strchr(delim, (unsigned)buffer[0])) {
				found = YES;
			}

		} else {
			break;
		}
	}

	return found == YES;
}

int String_read_bytes(String s, size_t offset,
                      FILE *f, size_t bytes)
{
	int b;
	void *old_raw;

	assert(s != NULL);

	old_raw = resize_and_cpy(s, offset + bytes, offset);

	if (NULL == old_raw) {
		return -1;
	}

	free(old_raw);
	s->len = offset;

	while(bytes) {
		bytes--;

		if ((b = fgetc(f)) != EOF) {
			s->raw[s->len++] = (unsigned char)b;

		} else {
			bytes++;
			break;
		}
	}

	s->raw[s->len++] = '\0';
	return bytes == 0;
}

static String String_alloc(void)
{
	String s;

	if ((s = malloc(sizeof(*s))) == NULL) {
		return NULL;
	}

	s->raw = NULL;
	s->size = 0;
	s->len = 0;
	s->resizable = YES;
	return s;
}

/* http://graphics.stanford.edu/~seander/bithacks.html#RoundUpPowerOf2 */
static size_t round_up_to_the_next_power_of_2(size_t num)
{
	num--;
	num |= num >> 1;
	num |= num >> 2;
	num |= num >> 4;
	num |= num >> 8;
	num |= num >> 16;
	num++;
	return  num += (num == 0);
}

static char *resize(String s, size_t size)
{
	char *old_raw;

	if (!resizable(s)) {
		return NULL;
	}

	size = round_up_to_the_next_power_of_2(size);
	old_raw = s->raw;

	if ((s->raw = malloc(size)) == NULL) {
		s->raw = old_raw;
		return NULL;
	}

	s->size = size;

	if (old_raw == NULL) {
		return s->raw;
	}

	return old_raw;
}

static char *resize_and_cpy(String s, size_t size, size_t keep)
{
	char *old_raw;

	if ((old_raw = resize(s, size)) == NULL) {
		return NULL;
	}

	/*
	 * raw[len] is uninitialized because len == (strlen(raw) + 1), so
	 *
	 * if len == keep:
	 *     means len bytes will be kept untouched.
	 *
	 * if len < keep:
	 *     means len bytes will be kept untouched and (keep - len) bytes
	 *     have to be zeroed.
	 *
	 * if len > keep:
	 *     means that keep bytes will be kept untouched.
	 */
	if (s->len > keep) {
		memmove(s->raw, old_raw, keep);

	} else {
		memmove(s->raw, old_raw, s->len);

		if (s->len < keep) {
			memset(s->raw + s->len, 0, keep - s->len);
		}
	}

	return old_raw;
}

#if !(_XOPEN_SOURCE >= 700 || _POSIX_C_SOURCE >= 200809L)
static size_t strnlen(const char *s, size_t n)
{
	size_t i = 0;

	while (s[i] != '\0' && i < n) {
		i++;
	}

	return i;
}
#endif
